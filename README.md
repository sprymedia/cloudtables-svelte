
# CloudTables Svelte

CloudTables Svelte is a Svelte component that adds custom HTML tags `<CloudTables/>` to your Svelte application.
These tags replace the script tags that are used to import your cloud table in vanilla HTML document.

[CloudTables](https://cloudtables.com/) is a no code / low code system which lets you create complex and dynamic database driven applications with ease. Hosted or self-hosted options are available so you can be up and running in moments.


## Getting Started

Inside your Svelte application install this component:

```sh
npm install --save @cloudtables/svelte
```

After npm has finished installing. Change your `App.js` file in `/src/` include

```js
import CloudTables from "@cloudtables/svelte";
```

Once those changes are made you can now use the custom CloudTables tags: `<CloudTables/>`, for example:

```html
<CloudTables
  src="https://ct-examples.cloudtables.io/loader/4e9e8e3c-f448-11eb-8a3f-43eceac3195f/table/d"
  apiKey="AzG0e04UxhduaTAJjYC3Dgfr"
/>
```

- `src` The custom url for your CloudTable.
- `apiKey` would be replaced by your API Key (see the _Security / API Keys_ section in your CloudTables application)
- `token` server side generated secure access token that can be used instead of an `apiKey`
- `userId` is optional, but will be used to uniquely identify user's in the CloudTables interface.
- `userName` is also optional, but can be used to help identify who made what changes when reviewing logs in CloudTables. It is recommended you include `userId` and `userName`.

All the data values required can be found in your [CloudTables](https://cloudtables.com/) application.
